package com.oreilly.servlet.multipart;

import java.io.InputStream;

public interface InputStreamCallBack {
    void callBack(InputStream inputStream);
}

